from application import application
import unittest

class FlaskAppTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        # creates a test client
        self.application = application.test_client()
        # propagate the exceptions to the test client
        self.application.testing = True

    def tearDown(self):
        pass

    def test_home_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.application.get('/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_home_data(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.application.get('/')

        # assert the response data
        self.assertEqual(result.data, '{"Hello": "World"}\n')

    def test_cpu_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.application.get('/cpu')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_cpu_percent_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.application.get('/cpupercent')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_memory_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.application.get('/memory')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_storage_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.application.get('/storage')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_platform_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.application.get('/platform')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

