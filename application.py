from flask import Flask
from flask_restful import Resource, Api

import resources
import os_platform
import os

application = Flask(__name__)
api = Api(application)

#For Memory status
api.add_resource(resources.getMemory,'/memory')
#For CPU times
api.add_resource(resources.getCPU,'/cpu')
#CPU utilization in percent
api.add_resource(resources.getCPUPercent,'/cpupercent')
#Root partition status
api.add_resource(resources.getStorage,'/storage')
#Front Page		
api.add_resource(resources.frontPage,'/')
#Platform Details
api.add_resource(os_platform.getPlatform,'/platform')

if __name__ == '__main__':
	application.run(port=8080,debug=True)
